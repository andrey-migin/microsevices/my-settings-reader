using MyYamlParser;
using NUnit.Framework;

namespace MySettingsReader.Tests
{
    public class TestsFlatDeserialization
    {

        [Test]
        public void TestFlatDeserialization()
        {
            /*
            var yamlText = "Version:2\n" +
                           "Field:\n" +
                           "  MyDataValue:value\n" +
                           "  MyDataValue2:2\n\n" +
                           "field2:asd\n" +
                           "MyBool: True\n" +
                           "EnumField: Two\n" +
                           "MyArray:\n" +
                           "  - value1\n" +
                           "  - value2";
            
            var yamlBytes = Encoding.UTF8.GetBytes(yamlText);

            var resultModel = SettingsReader.<MySettings>();
            Assert.AreEqual(2, resultModel.Version);
            Assert.AreEqual("value", resultModel.MyDataValue);
            
            Assert.AreEqual(2, resultModel.OtherValue);
            */
        }
    }


    public class MySettings
    {
        [YamlProperty]
        public int Version { get; set; }
        
        [YamlProperty("Field.MyDataValue")]
        public string MyDataValue { get; set; }
        
        [YamlProperty("Field.MyDataValue2")]
        public int OtherValue { get; set; }
        
    }
}